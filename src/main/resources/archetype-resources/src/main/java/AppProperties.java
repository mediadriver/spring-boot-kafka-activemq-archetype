#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Component;

@Component
@PropertySource("classpath:application.properties")
public class AppProperties {

	// Kafka Endpoint parameters
	@Value("${symbol_dollar}{kafka.url}")
	private String kafkaUrl;

	// AcrtiveMQ Properties
	@Value("${symbol_dollar}{amq.broker.url}")
	private String brokerUrl;

	@Value("${symbol_dollar}{amq.userName}")
	private String userName;

	@Value("${symbol_dollar}{amq.password}")
	private String password;

	@Value("${symbol_dollar}{amq.destination}")
	private String destination;

	public String getKafkaUrl() {
		return kafkaUrl;
	}

	public void setKafkaUrl(String kafkaUrl) {
		this.kafkaUrl = kafkaUrl;
	}

	public String getBrokerUrl() {
		return brokerUrl;
	}

	public void setBrokerUrl(String brokerUrl) {
		this.brokerUrl = brokerUrl;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}

}
