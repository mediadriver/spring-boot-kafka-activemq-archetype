#set( $symbol_pound = '#' )
#set( $symbol_dollar = '$' )
#set( $symbol_escape = '\' )
package ${package};

import org.apache.activemq.camel.component.ActiveMQComponent;
import org.apache.camel.CamelContext;
import org.apache.camel.CamelContextAware;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AppConfiguration implements CamelContextAware {

	@Autowired
	private AppProperties properties;

	private CamelContext context;

	/**
	 * ActiveMQ EndPoint Component
	 * 
	 * @return
	 */
	@Bean
	public ActiveMQComponent amq() {

		ActiveMQComponent amq = new ActiveMQComponent(context);
		amq.setBrokerURL(properties.getBrokerUrl());
		amq.setUserName(properties.getUserName());
		amq.setPassword(properties.getPassword());

		return amq;
	}
	
	public CamelContext getCamelContext() {
		return context;
	}

	public void setCamelContext(CamelContext context) {
		this.context = context;
	}
}
